
Personnal Info :

        Name : Ba Oumar
        Matricule : 000441023
        PS: My PAE is not available yet, so I decided to do the work by myself.

// Files explanation

1) generate_hashes.cpp : generete X number of hashes ( X given by user ) and store them in hashes_to_crack.txt
2) generate_RT.cpp:  generate RT tables of X rows (X given by the user) and store it in RT.txt
                    hashes : strored in hashes_RT.txt
                    passwords : stored in passwds_RT.txt

3) cracker.cpp : Read hashes from hashes_to_crack.txt.txt and check if available in RT.txt (hashes_RT.txt) ==> and return the succes rate.
4) (optionnal) password_checking.cpp : From an RT table compute the hash of the passwords and see if correspond to the given hash, return succes rate
5) passwd-utils2 : Contains used function.

// Code compilation  and Running

1) launch file compilator.sh by typing "./compilator.sh" in the terminal.
   This will compile all the objects file.

2) Code Running 

    In the terminal ty
    1) ./computes_hashes : to generate a table of hashes
    2) ./compute_RT      : to generate Rainbow table
    3) ./launch_attack    : to launch an attack to find the generated table of hashes in the Rainbow Table                






#include <iostream>
#include "passwd-utils2.hpp"
using namespace std;
int main(int argc, char *argv[])
{

	// generate RT table with c number of rows and store it in RT.txt

     int c;
    cout << "Enter the number of rows of RT to generete : \n";
    cin >> c;

	int minChar = 6;
	int maxChar = 10;
    std::string hashes_file;
    hashes_file = "hashes_RT.txt";
    std::string passwds_file;
    passwds_file = "passwds_RT.txt";


	rainbow::mass_generate(c, minChar, maxChar, hashes_file,passwds_file);
}

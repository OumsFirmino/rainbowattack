#include <iostream>
#include <iomanip>
#include "passwd-utils2.hpp"

int main(int argc, char *argv[])
{
    // Read hashes from hashes.txt, look if those hashes in provided RT  from RT.txt, return the percentage of hashes present in RT 

    std::string hashes_file;
    hashes_file ="hashes_to_crack.txt";
    std::string hashes_RT_file;
    hashes_RT_file = "hashes_RT.txt";
    std::string passwds_RT_file;
    passwds_RT_file = "passwds_RT.txt";
    
	double success = rainbow::passwd_cracker(hashes_file,hashes_RT_file);
	std::cout << std::setprecision(4) << success << "% success" << std::endl;
}

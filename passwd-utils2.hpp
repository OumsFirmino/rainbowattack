#include <string>
#include <fstream>
#include <sstream>
#include "random.hpp"
#include "sha256.h"
#include <stdio.h>



namespace rainbow {

std::string generate_passwd(int length)
{
	static const std::string char_policy = "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN1234567890";
	static const int c_len = char_policy.length();

	char str[length + 1];
	for(int i = 0; i < length; i++)
		str[i] = char_policy[rainbow::random(0, c_len - 1)];
	str[length] = '\0';

	return std::string(str);
}

void mass_generate(int n, int mc, int MC, const std::string& hashes_file, const std::string& passwords_file)
{
	std::ofstream passwds;
	passwds.open(passwords_file);
	std::ofstream hashes;
	hashes.open(hashes_file);


	if(passwds.is_open() && hashes.is_open())
	{		
		for(int i = 0; i < n; i++)

		{
			std::string pass = generate_passwd(rainbow::random(mc, MC));
            std::string hash = sha256(pass);
			

			
			hashes << hash << std::endl;
			passwds << pass << std::endl ;
	
				
			

		}

		passwds.close();
		hashes.close();
	}
	else
		throw std::runtime_error("Output files could not be opened");
}

inline bool check_pwd(const std::string& pwd, const std::string& hash)
{
	return sha256(pwd) == hash;
}

double mass_check(const std::string& hash_file,const std::string& passwd_file)
{
	std::ifstream passwds;
	std::ifstream hashes;
	passwds.open(passwd_file);
	hashes.open(hash_file);
	


	if(passwds.is_open() && hashes.is_open())
	{	
		
	
        std::string pass;
		std::string hash;

		int count = 0;
		int success = 0;
            while(std::getline(passwds, pass) && std::getline(hashes,hash))
		{
			

			count++;

			if(check_pwd(pass, hash))

				success++;
		}

		passwds.close();
		hashes.close();

		return (static_cast<double>(success) / count) * 100;
	}
	else
		throw std::runtime_error("Input files could not be opened");
}
double passwd_cracker(const std::string& hashes_to_crack_file,const std::string& hashes_RT_file)
{
	std::ifstream hashes;
	hashes.open(hashes_to_crack_file);
	


	if(hashes.is_open())

	{	

		std::string hash_to_crack;
		
		

		int count = 0;
		int success = 0;
	

            while(std::getline(hashes, hash_to_crack))

		{
			
			std::ifstream RT;
			RT.open(hashes_RT_file);

			
			count++;
	
			
            int count_RT = 0;
			std::string hash_RT;

			while(std::getline(RT,hash_RT)){

				count_RT++;
				


				if(hash_RT==hash_to_crack) {

					
					success++;
					
					
					}
			
		    

			}

			
		RT.close();	
		}
		
		hashes.close();
		
		
		return (static_cast<double>(success) / count) * 100;
	}
	else
		throw std::runtime_error("Input files could not be opened");
}
}//rainbow namespace

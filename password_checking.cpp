#include <iostream>
#include <iomanip>
#include "passwd-utils2.hpp"

int main(int argc, char *argv[])
{
    // check if password correspond to given hashes
    
    std::string hashes;
    filename ="hashes_RT.txt";
    std::string passwds;
    filename ="passwds_RT.txt";

	double success = rainbow::mass_check(hashes,passwds);
	std::cout << std::setprecision(4) << success << "% success" << std::endl;
}
